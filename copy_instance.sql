SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE pid <> pg_backend_pid() AND datname IN ('testbase', 'testbase_instance');
DROP DATABASE IF EXISTS testbase_instance;
CREATE DATABASE testbase_instance WITH TEMPLATE testbase;
